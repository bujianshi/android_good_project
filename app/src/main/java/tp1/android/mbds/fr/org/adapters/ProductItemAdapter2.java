package tp1.android.mbds.fr.org.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.List;
import java.util.Map;

import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.activities.R;
import tp1.android.mbds.fr.org.odt.Product;

/**
 * Created by loric on 14/12/2015.
 */
public class ProductItemAdapter2 extends BaseExpandableListAdapter {


    private Context context;
    private List<String> categories;
    private Map<String, List<Product>> produits;

    public ProductItemAdapter2(Context context, List<String> categories, Map<String, List<Product>> produits) {
        this.context = context;
        this.categories = categories;
        this.produits = produits;
    }

    @Override
    public int getGroupCount() {
        Log.w("group count", String.valueOf(categories.size()));
        return this.categories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.produits.get(this.categories.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.categories.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.produits.get(this.categories.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.product_group, null);
        }

        Log.w("group position", String.valueOf(groupPosition));
        String groupe = (String) getGroup(groupPosition);
        Log.w("group text", groupe);

        AQuery aq = new AQuery(convertView);
        aq.id(R.id.product_group_text_name).text(groupe);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        final Product product = (Product) getChild(groupPosition, childPosition);

        ProductViewHolder viewHolder = new ProductViewHolder();
        if(v == null){
            v = View.inflate(context, R.layout.product, null);
            viewHolder.name= (TextView)v.findViewById(R.id.product_name);
            viewHolder.img= (ImageView)v.findViewById(R.id.product_picture);
            viewHolder.desc= (TextView)v.findViewById(R.id.product_description);
            viewHolder.price= (TextView)v.findViewById(R.id.product_price);
            viewHolder.calorie= (TextView)v.findViewById(R.id.product_calories);
            viewHolder.type= (TextView)v.findViewById(R.id.product_type);
            viewHolder.discount= (TextView)v.findViewById(R.id.product_discount);
            v.setTag(viewHolder);
        }
        else{
            viewHolder = (ProductViewHolder) v.getTag();
        }
        String name = product.getName();
        String description = product.getDescription();
        String picture = product.getPicture();
        String price = String.valueOf(product.getPrice());
        String calories = String.valueOf(product.getCalories());
        String type = product.getType();
        String discount = String.valueOf(product.getDiscount());

        viewHolder.name.setText(name);
        //viewHolder.img.src;
        viewHolder.desc.setText(description);
        viewHolder.price.setText(price);
        viewHolder.calorie.setText(calories);
        viewHolder.type.setText(type);
        viewHolder.discount.setText(discount);

        AQuery aq = new AQuery(v);
        aq.id(viewHolder.img).image(picture);

        TextView productAddButton = (TextView) v.findViewById(R.id.product_add_button);

        productAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Singleton.getInstance().getCommande().add(product);
            }

        });

        return v;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    class ProductViewHolder{
        TextView name;
        ImageView img;
        //TextView button_add;
        TextView desc;
        TextView price;
        TextView calorie;
        TextView type;
        TextView discount;
    }
}
