package tp1.android.mbds.fr.org.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tp1.android.mbds.fr.org.SendCommandService;
import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.adapters.CommandeItemAdapter;
import tp1.android.mbds.fr.org.adapters.ProductItemAdapter2;
import tp1.android.mbds.fr.org.api.config.ApiAdresses;
import tp1.android.mbds.fr.org.odt.AsynMenu;

public class SeeCommandActivity extends AppCompatActivity {

    ListView listCommands;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_command);
        listCommands =  (ListView) findViewById(R.id.list_view_command);
        seeCommande();
    }

    public void seeCommande(){
        if(!Singleton.getInstance().getCommande().isEmpty()) {
            CommandeItemAdapter adapter = new CommandeItemAdapter(getApplicationContext(), Singleton.getInstance().getCommande());
            listCommands.setAdapter(adapter);
        }else{
            Toast.makeText(SeeCommandActivity.this, "Command is empty", Toast.LENGTH_LONG).show();
        }
    }


    public void sendCommand(View view){
        String[] params = new String[]{""};
        new AsynMenu(getApplicationContext()).execute(params);
    }

    public void menu(View view){
        Intent intent = new Intent(SeeCommandActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    public void doNothing(View view){
        if(Singleton.getInstance().getCommande().isEmpty()){
            Intent intent = new Intent(SeeCommandActivity.this, TakeCommandActivity2.class);
            Toast.makeText(SeeCommandActivity.this, "Command is empty", Toast.LENGTH_LONG).show();
            startActivity(intent);
        }
    }

}
