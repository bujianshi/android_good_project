package tp1.android.mbds.fr.org.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.androidquery.AQuery;

import tp1.android.mbds.fr.org.activities.R;
import tp1.android.mbds.fr.org.odt.Product;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Product product = (Product) getIntent().getSerializableExtra("product");

        AQuery aQuery = new AQuery(ProductActivity.this);
        aQuery.id(R.id.one_product_name).text(product.getName());
        aQuery.id(R.id.one_product_description).text(product.getDescription());
        aQuery.id(R.id.one_product_calories).text(String.valueOf(product.getCalories()));
        aQuery.id(R.id.one_product_discount).text(String.valueOf(product.getDiscount()));
        aQuery.id(R.id.one_product_price).text(String.valueOf(product.getPrice()));
        aQuery.id(R.id.one_product_type).text(product.getType());

        aQuery.id(R.id.one_product_picture).image(aQuery.getCachedImage(product.getPicture()));

    }

}
