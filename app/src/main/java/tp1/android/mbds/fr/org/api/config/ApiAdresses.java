package tp1.android.mbds.fr.org.api.config;

/**
 * Created by loric on 12/12/2015.
 */
public class ApiAdresses {

    private static final String urlBase = "http://92.243.14.22:1337/";

    public static String getPersonUrl(){
        return urlBase+"person/";
    }

    public static String getLoginUrl(){
        return getPersonUrl()+"login";
    }

    public static String getDeleteUrl(String id){
        return getPersonUrl()+id;
    }

    public static String getProductUrl(){
        return urlBase+"product/";
    }

    public static String getMenuUrl(){
        return urlBase+"menu/";
    }
}
