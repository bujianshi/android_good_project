package tp1.android.mbds.fr.org.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.List;

import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.activities.R;
import tp1.android.mbds.fr.org.odt.Product;

/**
 * Created by loric on 13/12/2015.
 */
public class ProductItemAdapter extends BaseAdapter {

    private Context context;
    public List<Product> productList;

    public ProductItemAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.productList = productList;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        ProductViewHolder viewHolder = new ProductViewHolder();
        if(v == null){
            v = View.inflate(context, R.layout.product, null);
            viewHolder.name= (TextView)v.findViewById(R.id.product_name);
            viewHolder.img= (ImageView)v.findViewById(R.id.product_picture);
            viewHolder.desc= (TextView)v.findViewById(R.id.product_description);
            viewHolder.price= (TextView)v.findViewById(R.id.product_price);
            viewHolder.calorie= (TextView)v.findViewById(R.id.product_calories);
            viewHolder.type= (TextView)v.findViewById(R.id.product_type);
            viewHolder.discount= (TextView)v.findViewById(R.id.product_discount);
            v.setTag(viewHolder);
        }
        else{
            viewHolder = (ProductViewHolder) v.getTag();
        }
        final Product product = productList.get(position);
        String name = product.getName();
        String description = product.getDescription();
        String picture = product.getPicture();
        String price = String.valueOf(product.getPrice());
        String calories = String.valueOf(product.getCalories());
        String type = product.getType();
        String discount = String.valueOf(product.getDiscount());

        viewHolder.name.setText(name);
        //viewHolder.img.src;
        viewHolder.desc.setText(description);
        viewHolder.price.setText(price);
        viewHolder.calorie.setText(calories);
        viewHolder.type.setText(type);
        viewHolder.discount.setText(discount);

        AQuery aq = new AQuery(v);
        aq.id(viewHolder.img).image(picture);

        TextView productAddButton = (TextView) v.findViewById(R.id.product_add_button);

        productAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Singleton.getInstance().getCommande().add(product);
            }

        });

        return v;
    }



    class ProductViewHolder{
        TextView name;
        ImageView img;
        //TextView button_add;
        TextView desc;
        TextView price;
        TextView calorie;
        TextView type;
        TextView discount;
    }

}
