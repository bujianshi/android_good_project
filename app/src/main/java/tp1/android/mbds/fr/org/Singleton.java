package tp1.android.mbds.fr.org;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tp1.android.mbds.fr.org.odt.Person;
import tp1.android.mbds.fr.org.odt.Product;

/**
 * Created by loric on 13/12/2015.
 */
public class Singleton {

    private List<Product> commande = new ArrayList<>();
    private Person user = new Person();

    private Singleton()
    {}

    /** Instance unique non préinitialisée */
    private static Singleton INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static synchronized Singleton getInstance()
    {
        if (INSTANCE == null)
        { 	INSTANCE = new Singleton();
        }
        return INSTANCE;
    }

    public synchronized List<Product> getCommande(){
        return this.commande;
    }

    public synchronized Person getUser(){
        return this.user;
    }

    public synchronized void setUser(Person p){
        this.user = p;
    }

    public synchronized String getJsonCommand(){
        String result = null;
        if(!Singleton.getInstance().getCommande().isEmpty()){
            int price = 0;
            int discount = 0;
            ArrayList<String> items = new ArrayList<>();

            for(Product product : Singleton.getInstance().getCommande()){
                price += product.getPrice();
                discount += product.getDiscount();
                items.add("{\"id\":\""+product.getId()+"\"}");
            }

            String cooker = "\"cooker\":{\"id\":\"568cfbaf4967e0714397a766\"}"; //il n'y a pas de web service pour les cooker, on va supposer qu'il n'y en a qu'un
            String server = "\"server\":{\"id\":\""+this.getUser().getId()+"\"}"; //serveur ayant recu la commande c'est a dire celui qui utilise l'application (donc celui qui est connecte)

            StringBuilder lStringBuilder =  new StringBuilder();
            lStringBuilder.append("{");
            lStringBuilder.append("\"price\":"+price+",");
            lStringBuilder.append("\"discount\":"+discount+",");
            lStringBuilder.append(server);
            lStringBuilder.append(",");
            lStringBuilder.append(cooker);
            lStringBuilder.append(",");
            lStringBuilder.append("\"items\":[");
            for(int i = 0; i < items.size() - 1; i++) {
                lStringBuilder.append(items.get(i));
                lStringBuilder.append(",");
            }
            lStringBuilder.append(items.get(items.size()-1));
            lStringBuilder.append("]");
            lStringBuilder.append("}");

            result =  lStringBuilder.toString();
        }
        return result;
    }

    public synchronized Map<String, Object> getMapCommand() {
        Map<String, Object> lMap = new HashMap<>();
        if (!Singleton.getInstance().getCommande().isEmpty()) {
            int price = 0;
            int discount = 0;
            ArrayList<String> items = new ArrayList<>();

            for (Product product : Singleton.getInstance().getCommande()) {
                price += product.getPrice();
                discount += product.getDiscount();
                items.add("{\"id\":\""+product.getId()+"\"}");
            }

            String server = "{\"id\":\"" + this.getUser().getId() + "\"}"; //serveur ayant recu la commande c'est a dire celui qui utilise l'application (donc celui qui est connecte)
            String cooker = "{\"id\":\"568cfbaf4967e0714397a766\"}"; //il n'y a pas de web service pour les cooker, on va supposer qu'il n'y en a qu'un

            lMap.put("price", price);
            lMap.put("discount", discount);
            lMap.put("server", server);
            lMap.put("cooker", cooker);

            StringBuilder lStringBuilder = new StringBuilder();
            lStringBuilder.append("[");
            for(int i = 0; i < items.size() - 1; i++) {
                lStringBuilder.append(items.get(i));
                lStringBuilder.append(",");
            }
            lStringBuilder.append(items.get(items.size()-1));
            lStringBuilder.append("]");

            lMap.put("items", lStringBuilder.toString());
        }
        return lMap;
    }

}
