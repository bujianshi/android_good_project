package tp1.android.mbds.fr.org.odt;

import java.io.Serializable;

/**
 * Created by Renyusan on 30/10/2015.
 */
public class Person implements Serializable{

    String id;
    String nom = "";
    String prenom;
    String sexe;
    String email;
    int num;
    boolean connected;

    public Person(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public boolean getConnected() { return connected;}

    public void setConnected(boolean connected){ this.connected = connected;}
}
