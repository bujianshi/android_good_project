package tp1.android.mbds.fr.org.odt;

import java.util.List;

/**
 * Created by loric on 14/12/2015.
 */
public class Menu {

    private int prince;
    private int discount;
    private String serverId;
    private String cookerId;
    private List<String> productsId;

    public int getPrince() {
        return prince;
    }

    public void setPrince(int prince) {
        this.prince = prince;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getCookerId() {
        return cookerId;
    }

    public void setCookerId(String cookerId) {
        this.cookerId = cookerId;
    }

    public List<String> getProductsId() {
        return productsId;
    }

    public void setProductsId(List<String> productsId) {
        this.productsId = productsId;
    }
}
