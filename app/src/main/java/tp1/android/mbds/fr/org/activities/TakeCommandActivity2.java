package tp1.android.mbds.fr.org.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.activities.R;
import tp1.android.mbds.fr.org.adapters.CommandeItemAdapter;
import tp1.android.mbds.fr.org.adapters.ProductItemAdapter;
import tp1.android.mbds.fr.org.adapters.ProductItemAdapter2;
import tp1.android.mbds.fr.org.api.config.ApiAdresses;
import tp1.android.mbds.fr.org.odt.AsynMenu;
import tp1.android.mbds.fr.org.odt.Product;

public class TakeCommandActivity2 extends AppCompatActivity {

    ExpandableListView listProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_command2);
        listProducts = (ExpandableListView) findViewById(R.id.expandable_list_view_products);
        getAllProduct();
    }

    public void getAllProduct(){
        final AQuery aQuery = new AQuery(TakeCommandActivity2.this);
        String url = ApiAdresses.getProductUrl();
        Log.w("url", url);

        aQuery.ajax(url, JSONArray.class, new AjaxCallback<JSONArray>() {

            @Override
            public void callback(String url, JSONArray json, AjaxStatus status) {
                if (json != null) {
                    if (status.getCode() == 200) {
                        List<String> types = new ArrayList<>();
                        List<Product> products;
                        Map<String, List<Product>> productsGroup = new HashMap<>();


                        try {
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject ob = json.getJSONObject(i);
                                Product p = new Product();
                                try {
                                    p.setName(ob.getString("name"));
                                    p.setPicture(ob.getString("picture"));
                                    String desc = ob.getString("description");
                                    String description = desc.replace("\\n", " ");
                                    p.setDescription(description);
                                    p.setPrice(ob.getInt("price"));
                                    p.setCalories(ob.getInt("calories"));
                                    p.setType(ob.getString("type"));
                                    p.setDiscount(ob.getInt("discount"));
                                    p.setId(ob.getString("id"));


                                    String type = p.getType();

                                    if (!types.contains(type)) {
                                        types.add(type);
                                    }

                                    if(!productsGroup.containsKey(type)) {
                                        productsGroup.put(type, new ArrayList<Product>());
                                    }

                                    products = productsGroup.get(type);
                                    products.add(p);
                                    productsGroup.put(type, products);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        for(String t : types) {
                            Log.w("type", t);
                        }

                        ProductItemAdapter2 adapter = new ProductItemAdapter2(TakeCommandActivity2.this, types, productsGroup);
                        //adapter = new ProductItemAdapter(getApplicationContext(), productsGroup);
                        listProducts.setAdapter(adapter);
                    } else {
                        Toast.makeText(aQuery.getContext(), "Error:" + status.getCode() + " Message :" + status.getMessage(), Toast.LENGTH_LONG).show();
                        aQuery.ajaxCancel();
                    }
                } else {
                    Toast.makeText(aQuery.getContext(), "Error:" + status.getCode() + " Message :" + status.getMessage(), Toast.LENGTH_LONG).show();
                    aQuery.ajaxCancel();
                }
            }
        }.method(AQuery.METHOD_GET));
    }



    public void seeCommande(View view){
        if(!Singleton.getInstance().getCommande().isEmpty()) {
            Intent intent = new Intent(TakeCommandActivity2.this, SeeCommandActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(TakeCommandActivity2.this, "Command is empty", Toast.LENGTH_LONG).show();
        }
    }

    public void sendCommande(View view){
        String[] params = new String[]{""};
        new AsynMenu(getApplicationContext()).execute(params);
    }

    public void menu(View view){
        Intent intent = new Intent(TakeCommandActivity2.this, MenuActivity.class);
        startActivity(intent);
    }

}
