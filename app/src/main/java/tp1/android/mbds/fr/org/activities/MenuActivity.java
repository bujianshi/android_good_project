package tp1.android.mbds.fr.org.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.odt.Person;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Intent intent = getIntent();
//        Person p  = (Person) intent.getSerializableExtra("toto");
    }

    public void allPerson(View view){
        Intent intent = new Intent(MenuActivity.this, AllPersonActivity.class);
        startActivity(intent);
    }

    public void takeCommand(View view){
        Intent intent = new Intent(MenuActivity.this, TakeCommandActivity.class);
        startActivity(intent);
    }

    public void takeCommand2(View view){
        Intent intent = new Intent(MenuActivity.this, TakeCommandActivity2.class);
        startActivity(intent);
    }

    public void seeCommand(View view){
        if(Singleton.getInstance().getCommande().isEmpty()){
            Toast.makeText(MenuActivity.this, "Command is empty", Toast.LENGTH_LONG).show();
        }else {
            Intent intent = new Intent(MenuActivity.this, SeeCommandActivity.class);
            startActivity(intent);
        }
    }
}
