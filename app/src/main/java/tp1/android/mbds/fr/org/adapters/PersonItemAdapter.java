package tp1.android.mbds.fr.org.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tp1.android.mbds.fr.org.activities.AllPersonActivity;
import tp1.android.mbds.fr.org.api.config.ApiAdresses;
import tp1.android.mbds.fr.org.odt.Person;
import tp1.android.mbds.fr.org.activities.R;

/**
 * Created by Renyusan on 30/10/2015.
 */


public class PersonItemAdapter extends BaseAdapter {

    private Context context;
    public List<Person> persons;

    public PersonItemAdapter(Context context, List<Person> person) {
        this.context = context;
        this.persons = person;
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int arg0) {
        return persons.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        View v = convertView;

        PersonViewHolder viewHolder = new PersonViewHolder();
        if(v == null){
            v = View.inflate(context, R.layout.osef, null);
            viewHolder.nom_prenom= (TextView)v.findViewById(R.id.textViewName);
            viewHolder.connected= (TextView)v.findViewById(R.id.textViewConnected);
            v.setTag(viewHolder);
        }
        else{
            viewHolder = (PersonViewHolder) v.getTag();
        }
        final Person person = persons.get(position);
        String nom = person.getPrenom() + " " + person.getNom();
        boolean isConnected = person.getConnected();
        String connected = "";
        if(isConnected)
            connected = "Connecté";
        else
            connected =  "Non Connecté";
        viewHolder.nom_prenom.setText(nom);
        viewHolder.connected.setText(connected);
        //viewHolder.date_creation.setText(comment.getName());

        TextView deletePersonTire = (TextView) v.findViewById(R.id.delete_person_tire);

        deletePersonTire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final AQuery aQuery = new AQuery(v);
                final String idToDelete = person.getId();
                String url = ApiAdresses.getDeleteUrl(idToDelete);

                aQuery.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {

                    @Override
                    public void callback(String url, JSONObject json, AjaxStatus status) {
                        if(json != null){
                            if(status.getCode() == 200) {
                                String message = "Utilisateur " + person.getPrenom()+" "+person.getNom() +" supprimé";
                                persons.remove(position);
                                PersonItemAdapter.this.notifyDataSetChanged();
                                Toast.makeText(aQuery.getContext(), message, Toast.LENGTH_LONG).show();
                                return;
                            }else{
                                Toast.makeText(aQuery.getContext(), "Error:" + status.getCode() + " Message :" + status.getMessage(), Toast.LENGTH_LONG).show();
                                aQuery.ajaxCancel();
                                return;
                            }
                        }else{
                            Toast.makeText(aQuery.getContext(), "Error:" + status.getCode() + " Message :" + status.getMessage(), Toast.LENGTH_LONG).show();
                            aQuery.ajaxCancel();
                            return;
                        }
                    }
                }.method(AQuery.METHOD_DELETE));
            }

        });


        return v;
    }

    class PersonViewHolder{
        TextView nom_prenom;
        TextView connected;
        //TextView date_creation;
    }

}
