package tp1.android.mbds.fr.org.odt;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.activities.MenuActivity;
import tp1.android.mbds.fr.org.activities.R;
import tp1.android.mbds.fr.org.api.config.ApiAdresses;

/**
 * Created by loric on 10/01/2016.
 */


public class AsynMenu extends AsyncTask<String, Void, String> {

    private URI url;
    private Context context;

    public AsynMenu(Context context) {
        this.context = context;
        try {
            //url = new URI("http://92.243.14.22/person/");
            url = new URI(ApiAdresses.getMenuUrl());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        try{
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            // add header

            Map<String, Object> command = Singleton.getInstance().getMapCommand();
            StringEntity entity = new StringEntity(Singleton.getInstance().getJsonCommand());
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

        } catch (Exception e){
        }
        return null;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String response){
        super.onPostExecute(response);
        Toast.makeText(this.context, "Commande envoyee", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this.context, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}

