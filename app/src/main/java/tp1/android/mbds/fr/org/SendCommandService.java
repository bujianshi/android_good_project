package tp1.android.mbds.fr.org;

import java.util.ArrayList;

import tp1.android.mbds.fr.org.odt.Product;

/**
 * Created by loric on 10/01/2016.
 */
public class SendCommandService {

    public SendCommandService(){

    }

    public String getJsonCommand(){
        String result = null;
        if(!Singleton.getInstance().getCommande().isEmpty()){
            int price = 0;
            int discount = 0;
            ArrayList<String> items = new ArrayList<>();

            for(Product product : Singleton.getInstance().getCommande()){
                price += product.getPrice();
                discount += product.getDiscount();
                items.add("{\"id\":"+product.getId()+"}");
            }

            String cooker = "\"cooker\":{\"id\":876543}"; //il n'y a pas de web service pour les cooker, on va supposer qu'il n'y en a qu'un
            String server = "\"server\":{\"id\":"+Singleton.getInstance().getUser().getId()+"}"; //serveur ayant recu la commande c'est a dire celui qui utilise l'application (donc celui qui est connecte)

            StringBuilder lStringBuilder =  new StringBuilder();
            lStringBuilder.append("{");
            lStringBuilder.append("\"price\":"+price+",");
            lStringBuilder.append("\"discount\":"+discount+",");
            lStringBuilder.append(server);
            lStringBuilder.append(",");
            lStringBuilder.append(cooker);
            lStringBuilder.append(",");
            lStringBuilder.append("\"items\":[");
            for(int i = 0; i < items.size() - 1; i++) {
                lStringBuilder.append(items.get(i));
                lStringBuilder.append(",");
            }
            lStringBuilder.append(items.get(items.size()-1));
            lStringBuilder.append("]");
            lStringBuilder.append("}");

            result =  lStringBuilder.toString();
        }
        return result;
    }
}
