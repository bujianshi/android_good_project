package tp1.android.mbds.fr.org.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.adapters.CommandeItemAdapter;
import tp1.android.mbds.fr.org.adapters.ProductItemAdapter;
import tp1.android.mbds.fr.org.api.config.ApiAdresses;
import tp1.android.mbds.fr.org.odt.Product;

public class TakeCommandActivity extends AppCompatActivity {



    ListView listProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_command);
        listProducts = (ListView) findViewById(R.id.listViewProduct);
        getAllProduct(this.getBaseContext());
    }

    public void getAllProduct(Context context){
        final AQuery aQuery = new AQuery(context);
        String url = ApiAdresses.getProductUrl();
        Log.w("url", url);

        aQuery.ajax(url, JSONArray.class, new AjaxCallback<JSONArray>() {

            @Override
            public void callback(String url, JSONArray json, AjaxStatus status) {
                if (json != null) {
                    if (status.getCode() == 200) {
                        List<Product> products = new ArrayList<>();
                        try {
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject ob = json.getJSONObject(i);
                                Product p = new Product();
                                try {
                                    p.setName(ob.getString("name"));
                                    p.setPicture(ob.getString("picture"));
                                    String desc = ob.getString("description");
                                    String description = desc.replace("\\n", " ");
                                    p.setDescription(description);
                                    p.setPrice(ob.getInt("price"));
                                    p.setCalories(ob.getInt("calories"));
                                    p.setType(ob.getString("type"));
                                    p.setDiscount(ob.getInt("discount"));
                                    products.add(p);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        ProductItemAdapter adapter = new ProductItemAdapter(getApplicationContext(), products);
                        listProducts.setAdapter(adapter);
                    } else {
                        Toast.makeText(aQuery.getContext(), "Error:" + status.getCode() + " Message :" + status.getMessage(), Toast.LENGTH_LONG).show();
                        aQuery.ajaxCancel();
                    }
                } else {
                    Toast.makeText(aQuery.getContext(), "Error:" + status.getCode() + " Message :" + status.getMessage(), Toast.LENGTH_LONG).show();
                    aQuery.ajaxCancel();
                }
            }
        }.method(AQuery.METHOD_GET));
    }

    public void seeCommande(View view){
        if(!Singleton.getInstance().getCommande().isEmpty()) {
            CommandeItemAdapter adapter = new CommandeItemAdapter(getApplicationContext(), Singleton.getInstance().getCommande());
            listProducts.setAdapter(adapter);
        }else{
            Toast.makeText(TakeCommandActivity.this, "Command is empty", Toast.LENGTH_LONG).show();
        }
    }

    public void sendCommande(View view){
        //TODO ajax post .../menu
        if(!Singleton.getInstance().getCommande().isEmpty()) {
            Singleton.getInstance().getCommande().clear();
            Toast.makeText(TakeCommandActivity.this, "Command sended", Toast.LENGTH_LONG).show();
            menu(view);
        }else{
            Toast.makeText(TakeCommandActivity.this, "Command is empty", Toast.LENGTH_LONG).show();
        }


    }

    public void menu(View view){
        Intent intent = new Intent(TakeCommandActivity.this, MenuActivity.class);
        startActivity(intent);
    }

}
